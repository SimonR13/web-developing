<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css" />
  <script>
  $(document).ready(function() {
		$('a').each(function() {
			var $link = $(this);
			  var $dialog2 = $('<div></div>')
			  	.load('dialog.jsp')
			  	.dialog({
			  		autoOpen: false,
			  		title: 'TestDialog',
			  		width: 500,
					height: 300,
					buttons: {
						"Close": function() {
							$dialog2.dialog('close');
						}
					}
		  		});
		  	$link.click(function() {
		  		$dialog2.dialog('open');
		  		return false;
		  	});
		  });
	  });
  </script>
<title>Start</title>
</head>
<body bgcolor='white' text='#0f0f0f'>
<form method="post" name="">
<table id="table">
	<tr><td><a href="#">einmal</a></td></tr>
	<tr><td><a href="#">zweimal</a></td></tr>
	<tr><td><a href="#">dreimal</a></td></tr>
</table>
</form>
</body>
</html>