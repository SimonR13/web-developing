package taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.*;
import java.util.*;

public class RadioTagHandler extends SimpleTagSupport {
    private String[] radioList=null;
    private String radioString=null;
    private String name;
    private static final String ATTR_TEMPLATE = "<tr><td id=\"%s\"><a href=\"#\" id=\"%s\" >%s</a></td></tr>"; 
    		//"<td><input type='radio' name='action' value='%s'>Stunden %s</input></td> ";
    
    public void setRadiosList(String value) {
            this.radioString = value;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public void doTag() throws JspException, IOException {
        PageContext pageContext = (PageContext) getJspContext();
        JspWriter out = pageContext.getOut();
//        this.radioList = this.getStringList(radioString);
        this.radioList = radioString.split(";");
        for (int i=0; i<this.radioList.length; i++ ) {
            String radioTag = String.format(ATTR_TEMPLATE, i, radioList[i], radioList[i]);
            out.println(radioTag);
        }
    }
    
//    private List<String> getStringList(String radioString) {
//    	String[] temp;
//		temp = radioString.split(";");
//    	for (int i=0; i<temp.length; i++) {
//			radioList.add(temp[i]);
//    	}
//    	return radioList;
//    }
}
