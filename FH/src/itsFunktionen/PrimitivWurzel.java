package itsFunktionen;

public class PrimitivWurzel {
	public long[] MakePrimitivListe(long Basis, int Prim) {
		long[] Zahlenliste=new long[Prim]; //, NullZuPrim=new long[Prim];
		for (int i=0; i<Prim; i++){
			Zahlenliste[i] = (long)Math.pow(Basis, i) % Prim;
		}
		return Zahlenliste;
	}
	
	public PrimitivListe PrimitivWurzelCheck(long Basis, int Prim) {
		long[] PrimListe = MakePrimitivListe(Basis, Prim);
		boolean PrimitivWurzel=false;
		for (int i=0; i < Prim; i++) {
			for (int j=0; j<=PrimListe.length; j++) {
				if (i==PrimListe[j]) { PrimitivWurzel = true; break;}
				else if (i!=PrimListe[j]) { PrimitivWurzel = false; }
			}
		}
		PrimitivListe PrimList = new PrimitivListe(PrimitivWurzel,PrimListe);
		return (PrimList);
	}
	
	public long DiskreterLogarithmus(long g, long a, long p) {
		long e;
		//g^x = a & x = logg(a) => 3^(log3(4)) mod 7
		double log = (Math.log(g) / Math.log(a));
		e = (long)(Math.pow(g, log) % p);
		return e;
	}

}
