package itsFunktionen;

public class DiffieHellman {

	public double getOneKey(long Basis, int Exponent, long Prim) {
		double Key=0;
		if (Exponent==1) { return (Basis % Prim); }
		else if (Exponent == 0) { return 0; }
		else {
			Key = Math.pow(Basis, Exponent) % Prim;
		}
		return Key;
	}
	public double getKey(long Basis, int ExponentA, int ExponentB, long Prim) {
		double Key;
		if (ExponentA==0 || ExponentB==0) { return (1 % Prim); }
		else if (Basis == 0) { return 0; }
		else {
			Key = Math.pow(Basis, (ExponentA * ExponentB)) % Prim;
		}
		return Key;
	}
}
