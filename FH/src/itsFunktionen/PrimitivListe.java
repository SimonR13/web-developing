package itsFunktionen;

public class PrimitivListe {
	private boolean PrimitivWurzel;
	private long[] Zahlenfolge;
	
	public PrimitivListe(boolean PrimWurzel, long Zahlenfolge[]) {
		this.setPrimitivWurzel(PrimWurzel);
		this.setZahlenfolge(Zahlenfolge);
	}
	
	public PrimitivListe(boolean PrimWurzel) {
		this.setPrimitivWurzel(PrimWurzel);
	}

	public boolean isPrimitivWurzel() {
		return PrimitivWurzel;
	}

	public void setPrimitivWurzel(boolean primitivWurzel) {
		PrimitivWurzel = primitivWurzel;
	}

	public long[] getZahlenfolge() {
		return Zahlenfolge;
	}

	public void setZahlenfolge(long[] zahlenfolge) {
		Zahlenfolge = zahlenfolge;
	}
	
	

}
